import pytest
from viterbi_aligner import ViterbiAligner


def reading_fasta_TEST():
    assert ViterbiAligner.get_sequence_from_file('data/seq1.fasta') == 'CA-T'


def full_alignment_TEST():
    aligner = ViterbiAligner('data/config.json')
    test_seq = ViterbiAligner.get_sequence_from_file("data/seq1.fasta")
    assert aligner(test_seq) == 'CA--T'


def config_parsing_TEST():
    try:
        ViterbiAligner('data/broken_config.json')
    except KeyError:
        assert True
    else:
        assert False


if __name__ == "__main__":
    reading_fasta_TEST()
    full_alignment_TEST()
    config_parsing_TEST()