from viterbi_aligner import ViterbiAligner

if __name__ == '__main__':
    aligner = ViterbiAligner('/Users/murk/Desktop/bioinf/labs/bioinfspbstu/Labs/lab2_viterbi/data/config.json')
    # aligner.print_table()
    test_seqs = [ViterbiAligner.get_sequence_from_file(f"/Users/murk/Desktop/bioinf/labs/bioinfspbstu/Labs/lab2_viterbi/data/seq{i}.fasta") for i in range(1, 5)]
    for seq in test_seqs:
        seq, path, prob = aligner(seq)
        print(seq, path, prob)







