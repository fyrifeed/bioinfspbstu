import json
import logging
# import math
from Bio import SeqIO
import numpy as np

# In the cells: M - 0, I - 1, D - 2

class ViterbiAligner:
    def __init__(self, path_to_config):
        self.list_of_enums = ["seq", "E_M", "E_I", "MM", "MI", "MD", "IM", "ID", "II", "DD", "DI", "DM", "q"]
        with open(path_to_config, 'r') as file:
            self.main_dict = json.load(file)
        if self.main_dict["seq"] is None or len(self.main_dict["seq"]) == 0:
            raise ValueError
        self.seq = self.main_dict["seq"]
        self.probs = self.main_dict["q"]
        self.table = [[[0, 0, 0] for _ in range(len(self.seq) + 1)] for _ in range(4)]
        self.none_to_one = lambda x: x if x is not False else 1
        self.tracking = dict()
        self.del_positions = None
        if diff := len(set(self.list_of_enums).symmetric_difference(set(self.main_dict.keys()))):
            logging.info(f"Keys {diff} are required in config")
            raise KeyError

    @staticmethod
    def get_sequence_from_file(path_to: str) -> str:
        return SeqIO.read(path_to, 'fasta').seq

    def vM(self, i, j):
        eMi = self.none_to_one(self.main_dict["E_M"][self.seq[j - 1]][i - 1])
        aMM = self.none_to_one(self.main_dict['MM'][i - 1])
        aIM = self.none_to_one(self.main_dict['IM'][i - 1])
        aDM = self.none_to_one(self.main_dict['DM'][i - 1])

        state_vals = [
            self.table[i - 1][j - 1][0] + np.log(aMM),
            self.table[i - 1][j - 1][1] + np.log(aIM),
            self.table[i - 1][j - 1][2] + np.log(aDM)
        ]
        val1 = max(state_vals)

        res_vM = np.log(eMi / self.probs[self.seq[j - 1]]) + val1
        if not (i, j) in self.tracking.keys():
            self.tracking[(i, j)] = []
        self.tracking[(i, j)].append((i - 1, j - 1, np.argmax(state_vals), round(res_vM, 3)))
        return round(res_vM, 3)

    def vI(self, i, j):
        eIj = self.none_to_one(self.main_dict["E_I"][self.seq[j - 1]][i])
        aMI = self.none_to_one(self.main_dict['MI'][i])
        aII = self.none_to_one(self.main_dict['II'][i])
        aDI = self.none_to_one(self.main_dict['DI'][i])

        state_vals = [
            self.table[i][j - 1][0] + np.log(aMI),
            self.table[i][j - 1][1] + np.log(aII),
            self.table[i][j - 1][2] + np.log(aDI)
        ]

        val2 = max(state_vals)
        res_vI = np.log(eIj / self.probs[self.seq[j - 1]]) + val2
        if not (i, j) in self.tracking.keys():
            self.tracking[(i, j)] = []
        self.tracking[(i, j)].append((i, j - 1, np.argmax(state_vals), round(res_vI, 3)))

        return round(res_vI, 3)

    def vD(self, i, j):
        aMD = self.none_to_one(self.main_dict['MD'][i - 1])
        aID = self.none_to_one(self.main_dict['ID'][i - 1])
        aDD = self.none_to_one(self.main_dict['DD'][i - 1])
        state_vals = [self.table[i - 1][j][0] + np.log(aMD), self.table[i - 1][j][1] + np.log(aID),
                          self.table[i - 1][j][2] + np.log(aDD)]
        if not (i, j) in self.tracking.keys():
            self.tracking[(i, j)] = []
        self.tracking[(i, j)].append((i - 1, j, np.argmax(state_vals), round(max(state_vals), 3)))
        return round(max(state_vals), 3)

    def calculate_cell(self, i, j):
        return [self.vM(i, j), self.vI(i, j), self.vD(i, j)]

    def fulfill_table(self):
        self.table[0][0] = [0, -np.inf, -np.inf]
        for i in range(1, 4):
            self.table[i][0] = [-np.inf, -np.inf, self.vD(i, 0)]
        for j in range(1, len(self.seq) + 1):
            self.table[0][j] = [-np.inf, self.vI(0, j), -np.inf]
        for i in range(1, 4):
            for j in range(1, len(self.seq) + 1):
                self.table[i][j] = self.calculate_cell(i, j)

    def print_table(self) -> None:
        if len(self.tracking.keys()) < 1:
            self.fulfill_table()
        for row in self.table:
            print(row)


    def reconstruct_path(self):
        if len(self.tracking.keys()) < 1:
            self.fulfill_table()
        elif self.del_positions and len(self.del_positions) > 0:
            return
        next_tup = (len(self.table) - 1, len(self.table[0]) - 1)
        path = []
        while next_tup in self.tracking.keys():
            prev_cell = max(self.tracking[next_tup], key=lambda x: x[3])
            next_tup = (prev_cell[0], prev_cell[1])
            path.append(prev_cell)
        self.path = path[::-1]
        self.del_positions = []


    def __call__(self, input_seq):
        self.reconstruct_path()
        if len(self.del_positions) == 0:
            for idx, cell in enumerate(self.path):
                if idx > 0:
                    if cell[2] == 1 and self.path[idx - 1][2] in (0, 2):
                        self.del_positions.append(idx)
        counter = 0
        del_counter = 0
        res_seq = []
        residual = len(self.seq) - len(input_seq)
        assert residual >= 0
        while counter < len(self.seq) - residual:
            if del_counter < len(self.del_positions) and counter == self.del_positions[del_counter]:
                res_seq.append('-')
                del_counter += 1
            else:
                res_seq.append(input_seq[counter])
                counter += 1
        state_mapper = ['M', 'I', 'D']
        col, row = self.path[-1][:2]
        vM, vI, vD = self.table[col][row]
        MM = self.main_dict['MM'][3]
        MI = self.main_dict['MI'][3]
        IM = self.main_dict['IM'][3]
        II = self.main_dict['II'][3]
        DM = self.main_dict['DM'][3]
        DI = self.main_dict['DI'][3]

        return ''.join(res_seq), ''.join([state_mapper[el[2]] for el in self.path]), np.exp(max(vM + max(MM, MI), vI + max(IM, II), vD + max(DM, DI)))
