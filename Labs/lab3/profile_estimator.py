from typing import List, Dict
import json
import logging
from Bio import SeqIO

class ProfileEstimator:
    def __init__(self, align_seq: str, sequences: List[str], weights: Dict):
        self.nucliotides = ["A", "C", "G", "T"]
        self.align_seq = align_seq
        self.profile_seqs = sequences
        self.weights = weights
        self.state_dict = {
            "MM": [],
            "MI": [],
            "MD": [],
            "IM": [],
            "ID": [],
            "II": [],
            "DD": [],
            "DI": [],
            "DM": []
        }

    def calculate_generations(self):
        cols = []
        for idx, seq in enumerate(self.profile_seqs[0]):
            cols.append([nuc[idx] for nuc in self.profile_seqs])
        states = ['M' if seq.count('-') < (len(seq)) / 2 else 'I' for seq in cols]
        skips = [seq.count('-') for seq in cols]
        E_M = {}
        E_I = {}
        for nuc in self.nucliotides:
            E_M[nuc] = [0 if idx > 0 else None for idx in range(len(self.profile_seqs[0]))]
            E_I[nuc] = [0 for _ in range(len(self.profile_seqs[0]))]
        match_iter = 1
        insertion_iter = 0
        for idx, state in enumerate(states):
            col = [nuc[idx] for nuc in self.profile_seqs]
            if state == 'M':
                for nuc in self.nucliotides:
                    E_M[nuc][match_iter] = col.count(nuc)
                match_iter += 1
            else:
                for nuc in self.nucliotides:
                    E_I[nuc][insertion_iter] = col.count(nuc)
                    insertion_iter += 1

        for trans in self.state_dict.keys():
            self.state_dict[trans] = [0 for _ in range(len(self.profile_seqs[0]))]
        self.state_dict['MD'][-1] = None
        self.state_dict['ID'][-1] = None
        self.state_dict['DD'][-1] = None

        match_state_idx = 0
        match_state_idx_list = []
        for i in range(len(states)):
            if states[i] == "M":
                match_state_idx_list.append(match_state_idx)
                match_state_idx += 1
            else:
                match_state_idx_list.append(0)

        for idx in range(len(self.profile_seqs[1])):
            for state in self.state_dict:
                tmp = (len(self.profile_seqs)) - skips[idx]
                if state == 'MM' and states[idx] == 'I':
                    self.state_dict[state][idx] = (len(self.profile_seqs) - 1) - skips[idx + 1] - tmp
                elif (((states[idx] == 'M' and skips[idx] != 0) or (states[idx] == 'D') and (states[idx + 1] == 'I')
                       or (states[idx] == 'M' and skips[idx] != 0) and (states[idx + 1] == 'M') and (skips[idx + 1] != 0))
                      or (states[idx] == 'M') and (skips[idx] != 0) and (states[idx + 1] == 'M') and (skips[idx + 1] == 0)):
                    self.state_dict[state][match_state_idx_list[idx]] = skips[idx]
                else:
                    self.state_dict[state][idx] = tmp
        self.state_dict['E_I'] = E_I
        self.state_dict['E_M'] = E_M


    @staticmethod
    def get_estimator_from_config(filename):
        with open(filename, 'r') as f:
            cfg = json.load(f)
        if not {'align_seq', 'sequences', 'weights'} <= dict(cfg).keys():
            raise ValueError
        return ProfileEstimator(**cfg)

    @staticmethod
    def get_sequence_from_file(path_to: str) -> str:
        return SeqIO.read(path_to, 'fasta').seq

    @staticmethod
    def pseudocount_for_column(column):
        res_column = []
        sum = 0
        for idx, el in enumerate(column):
            if el is not None:
                sum += el + 1
        if sum == 0:
            return [None] * len(column)
        for idx, el in enumerate(column):
            if el is not None:
                res_column.append(round((el + 1.) / sum, 4))
            else:
                res_column.append(None)
        return res_column

    @staticmethod
    def cols_to_rows(cols):
        rows = []
        for i in range(len(cols[0])):
            rows.append([col[i] for col in cols])
        return rows


    def pseudo_count_of_table(self, list_of_names, dict_of_arrays):
        matrix = []
        for name in list_of_names:
            matrix.append(dict_of_arrays[name])

        transposed = ProfileEstimator.cols_to_rows(matrix)
        pseudo_counted = [ProfileEstimator.pseudocount_for_column(col) for col in transposed]
        pseudo_counted = ProfileEstimator.cols_to_rows(pseudo_counted)
        return {name: arr for name, arr in zip(list_of_names, pseudo_counted)}

    def laplace_pseudocount(self):
        M_pseudo = self.pseudo_count_of_table(['MM', 'MI', 'MD'], self.state_dict)
        I_pseudo = self.pseudo_count_of_table(['IM', 'ID', 'II'], self.state_dict)
        D_pseudo = self.pseudo_count_of_table(['DD', 'DI', 'DM'], self.state_dict)
        E_I_pseudo = self.pseudo_count_of_table(self.state_dict['E_I'].keys(), self.state_dict['E_I'])
        E_M_pseudo = self.pseudo_count_of_table(self.state_dict['E_M'].keys(), self.state_dict['E_M'])

        profile = {
            **M_pseudo,
            **I_pseudo,
            **D_pseudo,
            "E_I": {
                **E_I_pseudo
            },
            "E_M": {
                **E_M_pseudo
            }
        }
        return profile

    def save_profile(self, filename):
        self.calculate_generations()
        profile = self.laplace_pseudocount()
        with open(filename, 'w') as f:
            json.dump(profile, f)
        logging.info(f'Profile is ready, filename: {filename}')