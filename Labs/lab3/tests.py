import pytest
from profile_estimator import ProfileEstimator

def config_parsing_TEST():
    try:
        ProfileEstimator.get_estimator_from_config('broken_config.json')
    except KeyError:
        assert True
    else:
        assert False


if __name__ == "__main__":
    config_parsing_TEST()