from linear_aligner import LinearSpaceAligner

if __name__ == '__main__':
    seq1, seq2 = LinearSpaceAligner.get_sequence_from_file('example1.fasta'), LinearSpaceAligner.get_sequence_from_file('example2.fasta')
    # seq1, seq2 = 'VGAHAGEYGA', 'VNVEEAGG'
    lin = LinearSpaceAligner(50, 5)
    seq, aligned = lin(seq1, seq2)
    print(seq)
    print(aligned)



