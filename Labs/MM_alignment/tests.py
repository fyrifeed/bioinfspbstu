import pytest
from linear_aligner import LinearSpaceAligner

def reading_fasta_TEST():
    assert LinearSpaceAligner.get_sequence_from_file('fasta1.fasta') == 'TGAATC'

def full_alignment_TEST():
    str1, str2 = 'VGAHAGEYGA', 'VNVEEAGG'
    lin = LinearSpaceAligner(50, 5)
    _, aligned = lin(str1, str2)
    assert aligned == 'VNVE--EAGG'



if __name__=="__main__":
    reading_fasta_TEST()
    full_alignment_TEST()