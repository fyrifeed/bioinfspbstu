from typing import Any
import blosum
from Bio import SeqIO


class LinearSpaceAligner:
    def __init__(self, blosum_number: int, d: int) -> None:
        self.blosum = blosum.BLOSUM(blosum_number)
        self.d = d
        self.path = []

    @staticmethod
    def get_sequence_from_file(path_to: str) -> str:
        return SeqIO.read(path_to, 'fasta').seq
    
    def get_score(self, seq1: str, seq2: str) -> list:
        path = []
        len1, len2 = len(seq1), len(seq2)
        dp = [[0 for _ in range(len1 + 1)] for _ in range(2)]
        current_index = 0
        dp[0] = [-i * self.d for i in range(len1 + 1)]
        for i in range(len2 + 1):
            for j in range(len1 + 1):
                if i > 0 and j > 0:
                    score = self.blosum[seq1[j-1]][seq2[i-1]]
                    if dp[1-current_index][j-1] + score >= max(dp[1-current_index][j] - self.d,
                                                               dp[current_index][j-1] - self.d):
                        path.append((i - 1, j - 1))
                        dp[current_index][j] = dp[1-current_index][j-1] + score
                    elif dp[1-current_index][j] - self.d > max(dp[1-current_index][j-1] + score,
                                                               dp[current_index][j-1] - self.d):
                        path.append((i - 1, j))
                        dp[current_index][j] = dp[1-current_index][j] - self.d
                    else:
                        dp[current_index][j] = dp[current_index][j-1] - self.d
                        path.append((i, j - 1))
            current_index = 1 - current_index
            dp[current_index][0] = dp[1 - current_index][0] - self.d
        return dp[1-current_index], path
    
    def mid_cells_iteration(self, seq1, seq2, start_seq1, start_seq2, end_seq1, end_seq2) -> None:
        if end_seq2-start_seq2 <= 1:
            self.path.append((end_seq1, end_seq2))
            self.path.append((start_seq1, start_seq2))
            return
        if end_seq2 - start_seq2 < 1:
            return
        mid = (start_seq2+end_seq2) // 2
        prefix, path1 = self.get_score(seq1[start_seq1:end_seq1:], seq2[start_seq2:mid:])
        suffix, path2 = self.get_score(seq1[start_seq1:end_seq1:][::-1], seq2[mid:end_seq2:][::-1])
        max_score_index = -1
        max_score = -1
        for idx in range(len(prefix)):
            weight = prefix[idx] + suffix[-idx-1]
            if max_score_index == -1 or max_score <= weight:
                max_score_index = idx
                max_score = weight
        self.path.append((max_score_index+start_seq1, mid))
        self.mid_cells_iteration(seq1, seq2, start_seq1, start_seq2, max_score_index+start_seq1, mid)
        self.mid_cells_iteration(seq1, seq2, max_score_index+start_seq1, mid, end_seq1, end_seq2)

    def __reduce_duplicates_mid_cells(self):
        self.path = list(set(self.path))
    
    def get_mid_cells(self, seq1, seq2):
        len1 = len(seq1)
        len2 = len(seq2)
        self.mid_cells_iteration(seq1,seq2,0,0,len1,len2)
        self.__reduce_duplicates_mid_cells()
        self.path.sort(key=lambda x: (x[0], x[1]))

    def __call__(self, seq1: str, seq2: str) -> tuple:
        self.get_mid_cells(seq2, seq1)
        aligned = ''
        for idx, (i, _) in enumerate(self.path):
            if idx > 0:
                if self.path[idx - 1][0] == i:
                    aligned += '-'
                else:
                    aligned += seq2[i - 1]
        return seq1, aligned
